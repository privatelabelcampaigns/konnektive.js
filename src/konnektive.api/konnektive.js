﻿var https = require("https");
var EventEmitter = require("events").EventEmitter;

module.exports.transactionsEndpoint = {
    convertToString: function (obj) {
        var array = new Array();

        for (prop in obj) {
            array.push(prop + "=" + obj[prop]);
        }

        return array.join("&");
    },

    httpsGet: function (options, callback) {
        https.request(options, function (response) {
            var data = "";

            response.on("data", function (chunk) {
                data += chunk;
            });

            response.on("end", function () {
                callback(data);
            });
        }).end();
    },

    fetchTransactions: function (options, callback) {
        var args = this.convertToString(options);

        var webRequest = {
            host: "api.konnektive.com",
            path: ("/transactions/query/?" + args)
        };

        //console.log("sending request: " + webRequest.host + webRequest.path + " ...");
        this.httpsGet(webRequest, function (response) {
            callback(JSON.parse(response));
        });
    },

    getTransactionStream: function (options, brand) {
        var e = new EventEmitter();
        var args = this.convertToString(options);
        var parent = this;
        
        var responseHandler = function (response) {
            if (response.result === "SUCCESS") {
                e.emit("data", response);
                
                var page = parseInt(response.message.page);
                var n = parseInt(response.message.totalResults);
                var pageLimit = parseInt(response.message.resultsPerPage);
                var maxPages = Math.ceil((n / pageLimit));
                var haveMorePages = (page < maxPages);

                e.emit("log", ("response[ok]: " + "total=" + n + " | itemsOnPage=" + response.message.data.length + " | pageNo=(" + page + "/" + maxPages + ") | " + "brand=" + brand));
                
                if (haveMorePages) {
                    options.page = (page + 1);
                    parent.fetchTransactions(options, responseHandler);
                }
                else {
                    e.emit("end");
                }
            }
            else {
                e.emit("error", response);
            }
        };

        this.fetchTransactions(options, responseHandler);

        return e;
    }
};