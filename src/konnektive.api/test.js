﻿var konnektive = require("./konnektive.js");
var config = require("./config.json");
var assert = require("assert");



testGetTransactionStream();

function testGetTransactionStream() {
    var brand = config.brands[0];

    var args = {
        loginId: brand.loginId,
        password: brand.password,
        startDate: "3/22/2017",
        endDate: "3/24/2017",
        page: 5
    };

    var stream = konnektive.transactionsEndpoint.getTransactionStream(args, brand.name);
    
    stream.on("begin", function () {
        console.log("on begin: ");
    });

    stream.on("data", function (data) {
        //console.log("on data: " + data);
    });

    stream.on("log", function (msg) {
        console.log("log: " + msg);
    });

    stream.on("end", function () {
        console.log("on end:");
    });

    stream.on("error", function () {
        console.log("on error:");
    });
}

function testFetchTransactions() {
    var args = {
        loginId: config.novuderm.loginId,
        password: config.novuderm.password,
        startDate: "3/20/2017",
        endDate: "3/21/2017"
    };

    konnektive.transactionsEndpoint.fetchTransactions(args, function (response) {
        var expectedResult = "SUCCESS";
        var actualResult = response.result;

        assert.equal(actualResult, expectedResult, ("expected: '" + expectedResult + "' but was: '" + actualResult + "'"));
        console.log("retrieve transaction from konnektive.com: passed");
        console.log(response);
    });
}

