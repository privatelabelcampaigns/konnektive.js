CREATE TABLE `transaction` (
	`transactionId` INT NOT NULL PRIMARY KEY,
	`dateCreated` DATETIME,
	`campaignName` VARCHAR(64),
	`campaignId` INT,
	`affId` VARCHAR(64),
	`clientOrderId` VARCHAR(64),
	`emailAddress` VARCHAR(64),
	`cardType` VARCHAR(64),
	`totalAmount` DECIMAL(10, 2),
	`orderType` VARCHAR(64),
	`responseText` VARCHAR(64)
);

CREATE TABLE `item` (
	`transactionId` INT NOT NULL,
	`product` VARCHAR(64),
	`purchaseId` VARCHAR(64),
	`price` DECIMAL(10, 2),
	`shipping` DECIMAL(10, 2),
	`salesTax` DECIMAL(10, 2),
	`quantity` INT,
	FOREIGN KEY (`transactionId`) REFERENCES `transaction`(`transactionId`)
);