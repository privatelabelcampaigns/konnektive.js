﻿var EventEmitter = require("events").EventEmitter;
var konnektive = require("./konnektive.js");
var dateformatter = require("strftime");
var config = require("./config.json");
var mysql = require("mysql");

runOnce();

function runOnce() {
    var db = mysql.createConnection(config.database);
    fetchFieldsFromDatabase(db, function (transactionTableFields, itemTableFields) {
            var result = fetchTransactionsFromKonnektive(transactionTableFields, itemTableFields);

            result.on("end", function () {
                console.log("===== done =====");
            });
    });
}

function run() {
	console.log("will begin in " + config.queryIntervalInMinutes + " mins ...");
	
    var db = mysql.createConnection(config.database);
    fetchFieldsFromDatabase(db, function (transactionTableFields, itemTableFields) {
        setInterval(function () {
            var result = fetchTransactionsFromKonnektive(transactionTableFields, itemTableFields);

            result.on("end", function () {
            });
        }, (config.queryIntervalInMinutes * 60000));
    });
}

function fetchFieldsFromDatabase(connection, callback) {
    connection.query("DESCRIBE `transaction`;", function (error1, results1) {
        var transactionFields = new Array();
        for (i = 0; i < results1.length; i++) {
            transactionFields.push(results1[i].Field);
        }

        connection.query("DESCRIBE `item`;", function (error2, results2) {
            if (error2) {
                console.log("error occured: " + error2);
            }

            var itemFields = new Array();
            for (i = 0; i < results2.length; i++) {
                itemFields.push(results2[i].Field);
            }

            callback(transactionFields, itemFields);
            connection.end();
        });
    });
}

function fetchTransactionsFromKonnektive(transactionFields, itemFields) {
    var events = new EventEmitter();

    var brands = config.brands;
    var startDate = new Date();
    startDate.setDate(new Date().getDate() - config.numberOfDaysFromToday);
    var from = dateformatter("%m/%d/%Y", startDate);
    var to = dateformatter("%m/%d/%Y", new Date());

    console.log("querying konnektive data from: '" + from + "' to: '" + to + "' ...");

    for (var i = 0; i < brands.length; i++) {
        var brand = brands[i];
        var konnektiveArgs = {
            loginId: brand.loginId,
            password: brand.password,
            startDate: from,
            endDate: to
        };
        var stream = konnektive.transactionsEndpoint.getTransactionStream(konnektiveArgs, brand.name);

        stream.on("data", function (response) {
            var connection = mysql.createConnection(config.database);
            var transactions = response.message.data;

            try {
                for (var ii = 0; ii < transactions.length; ii++) {
                    var insertStatments = convertTransactionIntoSqlStatements(transactions[ii], transactionFields, itemFields);

                    for (var iii = 0; iii < insertStatments.length; iii++) {
                        var statment = insertStatments[iii];
                        connection.query(statment, onInsertStatementWasExecuted);
                    }
                }
            }
            finally {
                connection.end();
            }
        });

        stream.on("error", function (response) {
        });

        stream.on("end", function () {
            events.emit("end");
        });

        stream.on("log", function (message) {
            console.log("\t" + message);
        });
    }

    return events;
}

function onInsertStatementWasExecuted(error, results) {
    if (error) {
        console.log(error);
    }
    else {
        //console.log("sql: ok");
    }
}

function convertTransactionIntoSqlStatements(transaction, transactionFields, itemFields) {
    var statements = new Array();

    // build sql command for a transaction record
    statements.push(convertObjectIntoSqlStatement(transaction, "transaction", transactionFields));

    // build sql command for each item record.
    var items = transaction.items;
    for (var i = 0; i < items.length; i++) {
        var product = items[i];
        product["transactionId"] = transaction.transactionId;
        statements.push(convertObjectIntoSqlStatement(product, "item", itemFields));
    }
    //console.log("serialized transaction: " + transaction.transactionId);

    return statements;
}

function convertObjectIntoSqlStatement(obj, tableName, fields) {
    var validFields = new Array();
    var validValues = new Array();
    var value;

    for (prop in obj) {
        if (contains(fields, prop)) {
            validFields.push("`" + prop + "`");

            value = obj[prop];
            if (value !== null) { validValues.push("'" + (obj[prop]).toString().replace(/'/g, "''") + "'"); }
            else { validValues.push("null"); }
        }
    }

    return ("INSERT IGNORE INTO `" + tableName + "` (" + (validFields.join(",")) + ") VALUES (" + (validValues.join(",")) + ");");
}

function contains(array, item) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] === item) {
            return true;
        }
    };
    return false;
}